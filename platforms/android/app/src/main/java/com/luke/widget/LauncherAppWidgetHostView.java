//Based on https://github.com/willli666/Android-Trebuchet-Launcher-Standalone/blob/master/src/com/cyanogenmod/trebuchet/LauncherAppWidgetHostView.java

package com.luke.widget;

import android.appwidget.AppWidgetHostView;
import android.content.Context;
import android.os.SystemClock;
import android.view.MotionEvent;

import android.util.Log;

public class LauncherAppWidgetHostView extends AppWidgetHostView
{
    private float oldX;
    private float oldY;
    private int x_move = 0;
    private boolean is_move;
    
    private boolean mHasPerformedLongPress;
    private CheckForLongPress mPendingCheckForLongPress;
 
	private float newX;
	private float newY;
	
	private float tmp_x;
	private float tmp_y;
	
	
    public LauncherAppWidgetHostView(Context context) 
    {
        super(context);
    }

	public boolean dispatchTouchEvent(MotionEvent ev) 
	{
	
		if( Widget.seek_open == true)
		{
			return false;
		}
	
		switch( ev.getAction() )
		{
        case MotionEvent.ACTION_DOWN:
            oldX= ev.getX();
            oldY= ev.getY();
            x_move = 0;
            
            mHasPerformedLongPress = false;
            if (mPendingCheckForLongPress != null) { removeCallbacks(mPendingCheckForLongPress); }
            postCheckForLongClick();
            
            is_move = false;
            break;
            
        case MotionEvent.ACTION_MOVE:
			
            newX = ev.getX();
            newY = ev.getY();
            
            float deltaX = oldX - newX;
            float deltaY = oldY - newY;
			
			if( tmp_x != newX && tmp_y != newY )
			{
				mHasPerformedLongPress = false;
				if (mPendingCheckForLongPress != null) { removeCallbacks(mPendingCheckForLongPress); }
				
				postCheckForLongClick();
				
				if(Math.abs(deltaY)<Math.abs(deltaX))
				{
					removeCallbacks(mPendingCheckForLongPress);
					x_move = 1;
				}
				is_move = true;
			}
			else
			{
				is_move = false;
			}
			
			tmp_x = newX;
			tmp_y = newY;
			
            break;
            
        case MotionEvent.ACTION_UP:
            if (mPendingCheckForLongPress != null) { removeCallbacks(mPendingCheckForLongPress); }
            is_move = false;
			break;
			
        case MotionEvent.ACTION_CANCEL:
            is_move = false;
			if (mPendingCheckForLongPress != null) { removeCallbacks(mPendingCheckForLongPress); }
                break;
                
        }
     
		if (mHasPerformedLongPress==true)
		{
			//Channel all rest..
			long downTime = SystemClock.uptimeMillis();
			long eventTime = SystemClock.uptimeMillis();
			int action = MotionEvent.ACTION_CANCEL;
			MotionEvent event = MotionEvent.obtain(downTime, eventTime, action, oldX, oldY, 0);
			super.dispatchTouchEvent(event);
			return true;
		}
			
		Widget.pass_to_cordova(ev);
		
		if(x_move == 1)
		{
			//Canncel Event senden!
			long downTime = SystemClock.uptimeMillis();
			long eventTime = SystemClock.uptimeMillis();
			int action = MotionEvent.ACTION_CANCEL;
			MotionEvent event = MotionEvent.obtain(downTime, eventTime, action, oldX, oldY, 0);
			super.dispatchTouchEvent(event);

			return true;
		}
		else
		{
			if(Widget.show_btn==true)
			{
				super.requestDisallowInterceptTouchEvent(true);
			}
			else
			{
				super.requestDisallowInterceptTouchEvent(false);
			}
		
			Widget.show_scroll();
			
			return super.dispatchTouchEvent(ev);
		}
	}
	
  class CheckForLongPress implements Runnable 
  {
        private int mOriginalWindowAttachCount;

        public void run() {
            if ((getParent() != null) && hasWindowFocus()
                    && mOriginalWindowAttachCount == getWindowAttachCount()
                    && !mHasPerformedLongPress) {
                if (performLongClick()) {
                    mHasPerformedLongPress = true;
                }
            }
        }

        public void rememberWindowAttachCount()
        {
            mOriginalWindowAttachCount = getWindowAttachCount();
        }
    }

    private void postCheckForLongClick() 
    {
        mHasPerformedLongPress = false;

        if (mPendingCheckForLongPress == null) 
        {
            mPendingCheckForLongPress = new CheckForLongPress();
        }
        mPendingCheckForLongPress.rememberWindowAttachCount();
       
        postDelayed(mPendingCheckForLongPress, 1200);
    }

    @Override
    public void cancelLongPress() 
    {	
        super.cancelLongPress();

        mHasPerformedLongPress = false;
        if (mPendingCheckForLongPress != null)
        {
            removeCallbacks(mPendingCheckForLongPress);
        }
    }
}
