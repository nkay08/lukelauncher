
 
package com.luke.widget;

import android.content.Context;
import android.webkit.JavascriptInterface;

public class WebAppInterface {
    Context mContext;

    /** Instantiate the interface and set the context */
    WebAppInterface(Context c) {
        mContext = c;
    }

    @JavascriptInterface
    public void removeWidget(){
       Widget.removeWidget();
    }
    

    @JavascriptInterface
    public void add_widget(){
       Widget.add_widget();
    }
    
    @JavascriptInterface
    public void add_new_widget(String in_package , String in_class){
       Widget.add_new_widget(in_package , in_class);
    }
    
    
    @JavascriptInterface
    public void ok_view(){
       Widget.close();
    }
    
    
    @JavascriptInterface
    public void reset_view(){
       Widget.reset_view();
    }
    

    @JavascriptInterface
    public void close(){
       Widget.close();
    }    
    
    
    @JavascriptInterface
    public void set_height(String in_height)
    {
       Widget.set_height( Integer.parseInt( in_height) );
    }
    
}
