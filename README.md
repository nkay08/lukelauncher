# LukeLauncher  &nbsp; &nbsp;  ![Logo](https://gitlab.com/LukeSoftware/lukelauncher/-/raw/master/logo_small.png?inline=false)
A Open Source launcher for Android. Minimal, fast - categorie based!

#### Main features:
* Apps are managed in categories
* Suitable for left and right handers
* Notifications are supported
* Supports Tablets (Horizontal and vertical alignment possible)
* Quick search function
* Iconpacks can be used
* Multilingual (English, German and Spanish (By: Carlos Luna))

#### Technical:
Requirements for building:
* Nodejs and npm
* cordova (installed via npm)
* Java and the android sdk
